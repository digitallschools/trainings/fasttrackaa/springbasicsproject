/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.fullstackaa.spring.repo;

import com.digitallschool.fullstackaa.spring.model.Customer;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@Repository
public class CustomerRepository {

    public void addCustomer(Customer c) {

    }

    public Customer getCustomer(Integer customerId) {
        return null;
    }

    public List<Customer> getCustomers() {
        return null;
    }

    public Customer updateCustomer(Customer customer) {
        return null;
    }

    public Customer deleteCustomer(Integer customerId) {
        return null;
    }
}
