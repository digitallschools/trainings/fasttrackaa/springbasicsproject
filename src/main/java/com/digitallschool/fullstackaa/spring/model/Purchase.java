/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.fullstackaa.spring.model;

import javax.inject.Inject;
import javax.inject.Named;
import org.springframework.context.annotation.Scope;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
//@Component
@Named("tempPurchase")
@Scope("prototype")
public class Purchase {

    private Integer purchaseId;
    private double amount;

    //@Autowired
    //@EmptyCustomerQualifier
    @Inject
    //@Named("c1")
    private Customer customer;

    public Purchase() {

    }

    public Purchase(Integer purchaseId, double amount, Customer customer) {
        this.purchaseId = purchaseId;
        this.amount = amount;
        this.customer = customer;
    }

    public Integer getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(Integer purchaseId) {
        this.purchaseId = purchaseId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

}
