/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.fullstackaa.spring.model;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@Target(value = {TYPE, METHOD, FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Qualifier("full")
public @interface FullCustomerQualifier {

}
