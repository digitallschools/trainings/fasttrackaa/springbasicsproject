/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.fullstackaa.spring.model;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class Payment {

    private Integer paymentId;
    private double amount;

    //@Autowired
    //@Qualifier("empty")
    //@EmptyCustomerQualifier
    @Resource(name="c1")
    private Customer paidBy;

    public Payment() {

    }
    
    //@PostConstruct
    public void initialize(){
        System.out.println("Payment Setup");
    }
    
    //@PreDestroy
    public void destroyed(){
        System.out.println("Payment Setup Cleared");
    }

    public Payment(Integer paymentId, double amount, Customer paidBy) {
        this.paymentId = paymentId;
        this.amount = amount;
        this.paidBy = paidBy;
    }

    public Integer getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Customer getPaidBy() {
        return paidBy;
    }

    public void setPaidBy(Customer paidBy) {
        this.paidBy = paidBy;
    }

}
