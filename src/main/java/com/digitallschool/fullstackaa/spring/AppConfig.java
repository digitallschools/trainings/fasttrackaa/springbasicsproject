/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.fullstackaa.spring;

import com.digitallschool.fullstackaa.spring.model.Customer;
import com.digitallschool.fullstackaa.spring.model.Purchase;
import javax.inject.Named;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@Configuration
public class AppConfig {
    @Bean
    public Purchase emptyPurchase(){
        return new Purchase();
    }
    
    @Bean
    @Scope("prototype")
    @Primary
    //@Named("c1")
    public Customer emptyCustomer(){
        return new Customer();
    }
    
    @Bean
    @Scope("prototype")
    public Customer idBasedCustomer(){
        Customer temp =  new Customer();
        temp.setCustomerId(777);
        return temp;
    }
}
