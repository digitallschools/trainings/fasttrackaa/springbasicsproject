/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.fullstackaa.spring;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class Book {

    private Integer bookId;
    private String title;

    public Book() {
        super();
    }

    public Book(Integer bookId, String title) {
        this.bookId = bookId;
        this.title = title;
    }
    
    public void init(){
        System.out.println("Book Initialized");
    }
    
    public void destroy(){
        System.out.println("Book Setup Destroyed");
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
