/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.fullstackaa.spring;

import com.digitallschool.fullstackaa.spring.model.Purchase;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class AppJavaMain {
    public static void main(String[] args) {
        AbstractApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
        
        Purchase a = ctx.getBean(Purchase.class);
        
        System.out.println(a.getCustomer().getCustomerId());
    }
}
