/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.fullstackaa.spring;

import com.digitallschool.fullstackaa.spring.model.Customer;
import com.digitallschool.fullstackaa.spring.model.Payment;
import com.digitallschool.fullstackaa.spring.model.Purchase;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class AppXMLMain {

    public static void main(String[] args) {
        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("spring_config.xml");
        
        Purchase t = ctx.getBean("tempPurchase", Purchase.class);
        System.out.println(t);
        System.out.println(t.getCustomer().getCustomerId());
        
        Purchase t2 = ctx.getBean(Purchase.class);
        System.out.println(t2);
        System.out.println(t2.getCustomer().getCustomerId());
        
        ctx.registerShutdownHook();
    }
    
    public static void main4(String[] args) {
        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("spring_config.xml");
        
        Payment a = ctx.getBean(Payment.class);
        
        System.out.println(a.getPaidBy().getCustomerId());
        System.out.println(a.getPaymentId());
        
        ctx.registerShutdownHook();
    }
    
    public static void main3(String[] args) {
        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("spring_config.xml");

        Book b1 = ctx.getBean("book1", Book.class);
        System.out.println(b1.getBookId());
        System.out.println(b1.getTitle());

        Customer c1 = ctx.getBean("c1", Customer.class);
        System.out.println(c1.getName());
        
        ctx.registerShutdownHook();
    }

    public static void main2(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring_config.xml");

        Book b1 = ctx.getBean("book1", Book.class);
        Book b2 = ctx.getBean("book1", Book.class);

        System.out.println(b1.getBookId());
        System.out.println(b1.getTitle());
        System.out.println(b2.getBookId());
        System.out.println(b2.getTitle());
        System.out.println("#############################");

        b1.setBookId(890);

        System.out.println(b1.getBookId());
        System.out.println(b1.getTitle());
        System.out.println(b2.getBookId());
        System.out.println(b2.getTitle());

    }

    public static void main1(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring_config.xml");

        Book b1 = ctx.getBean("book1", Book.class);

        Book b2 = new Book();

        System.out.println(b2.getBookId());
        System.out.println(b2.getTitle());

        System.out.println(b1.getBookId());
        System.out.println(b1.getTitle());

        Book b3 = ctx.getBean("book2", Book.class);
        System.out.println(b3.getBookId());
        System.out.println(b3.getTitle());

    }
}
